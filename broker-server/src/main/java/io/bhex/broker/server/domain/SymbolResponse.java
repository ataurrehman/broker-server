package io.bhex.broker.server.domain;

import lombok.*;

import java.math.BigDecimal;

@Data
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderClassName = "Builder", toBuilder = true)
public class SymbolResponse {

    private Long id;
    private String symbolId;
    private Long exchangeId;

}
