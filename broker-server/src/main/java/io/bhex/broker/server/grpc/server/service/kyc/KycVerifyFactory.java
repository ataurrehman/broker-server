package io.bhex.broker.server.grpc.server.service.kyc;

import io.bhex.broker.common.exception.BrokerErrorCode;
import io.bhex.broker.common.exception.BrokerException;
import io.bhex.broker.grpc.user.kyc.*;
import io.bhex.broker.server.domain.UserVerifyStatus;
import io.bhex.broker.server.domain.kyc.KycLevelEnum;
import io.bhex.broker.server.grpc.server.service.BasicService;
import io.bhex.broker.server.grpc.server.service.kyc.impl.*;
import io.bhex.broker.server.model.BrokerKycConfig;
import io.bhex.broker.server.model.UserKycApply;
import io.bhex.broker.server.model.UserVerify;
import io.bhex.broker.server.primary.mapper.UserKycApplyMapper;
import io.bhex.broker.server.primary.mapper.UserVerifyMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class KycVerifyFactory {

    @Resource
    private BasicKycVerifyImpl basicKycVerify;

    @Resource
    @Qualifier("SeniorKycVerify")
    private SeniorKycVerifyImpl seniorKycVerify;

    @Resource
    @Qualifier("SeniorFaceKycVerify")
    private SeniorFaceKycVerifyImpl seniorFaceKycVerify;

    @Resource
    private SeniorFaceResultKycVerifyImpl seniorFaceResultKycVerify;


    @Resource
    protected UserKycApplyMapper userKycApplyMapper;

    @Resource
    private VipKycVerifyImpl vipKycVerify;

    @Resource
    private BasicService basicService;

    @Resource
    private UserVerifyMapper userVerifyMapper;

    public BasicKycVerifyResponse basicVerify(BasicKycVerifyRequest request) {
        return basicKycVerify.verify(request);
    }

    public PhotoKycVerifyResponse seniorVerify(PhotoKycVerifyRequest request) {
        UserVerify userVerify = userVerifyMapper.getByUserId(request.getHeader().getUserId());
        if (userVerify == null) {
            throw new BrokerException(BrokerErrorCode.KYC_BASIC_VERIFY_UNDONE);
        }

        if (isNeedFaceCompare(userVerify)) {
            return seniorFaceKycVerify.verify(request);
        } else {
            return seniorKycVerify.verify(request);
        }
    }

    public FaceKycVerifyResponse seniorFaceResultVerify(FaceKycVerifyRequest request) {
        return seniorFaceResultKycVerify.verify(request);
    }

    public VideoKycVerifyResponse vipVerify(VideoKycVerifyRequest request) {
        return vipKycVerify.verify(request);
    }

    private boolean isNeedFaceCompare(UserVerify userVerify) {
        BrokerKycConfig config = basicService.getBrokerKycConfig(userVerify.getOrgId(), userVerify.getNationality());
        if (config == null) {
            config = BrokerKycConfig.newDefaultInstance(userVerify.getOrgId());
        }
        return config.getSecondKycLevel() != 20;
    }
    public UserVerify getJumioStatus(Long userId){
        UserVerify userVerify= userVerifyMapper.getJumioRecentStatusByUserId(userId);
        return userVerify;
    }

    public Boolean createUserJumioData(UserKycApply userKycApply){
        try{

            UserVerify newUserVerify = new UserVerify();
            newUserVerify.setUserId(userKycApply.getUserId());
            newUserVerify.setKycLevel(userKycApply.getKycLevel());
            newUserVerify.setWorkFlowExecutionId(userKycApply.getWorkFlowExecutionId());
            newUserVerify.setAccountId(userKycApply.getAccountId());
            newUserVerify.setVerifyStatus(userKycApply.getVerifyStatus());
            newUserVerify.setKycLevel(userKycApply.getKycLevel());
            // newUserVerify.setCardNo("");
            // newUserVerify.setCardNoHash("");
            // newUserVerify.setCardBackUrl("");
            // newUserVerify.setCardFrontUrl("");
            // newUserVerify.setFirstName("");
            // newUserVerify.setSecondName("");
            // newUserVerify.setCardType(0);
            // newUserVerify.setNationality(0L);
          //  newUserVerify.setDataEncrypt(0);
            newUserVerify.setCreated(userKycApply.getCreated());
            newUserVerify.setUpdated(userKycApply.getCreated());


            UserVerify userVerifyEncrypted = UserVerify.encrypt(newUserVerify);

        // will return 1 on success handle this case
            userKycApplyMapper.insertSelective(userKycApply);
            // update data for both tables
            UserVerify oldRecord = userVerifyMapper.getByUserId(userKycApply.getUserId());
            if(oldRecord == null){
                userVerifyMapper.insertRecord(userVerifyEncrypted);
            }
            else{
                userVerifyMapper.update(userVerifyEncrypted);
            }

            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public Boolean updateUserJumioData(OrgUpdateKycInfo request){
        try{
            KycLevelEnum kycLevel = KycLevelEnum.BASIC;
            Long timestamp = System.currentTimeMillis();

            UserVerify userVerifyDb= userVerifyMapper.getByJumioAccountId(request.getAccountId());

            UserVerify updateUserVerify = new UserVerify();
            updateUserVerify.setUserId(userVerifyDb.getUserId());
            updateUserVerify.setOrgId(request.getHeader().getOrgId());
            updateUserVerify.setFirstName(request.getFirstName());
            updateUserVerify.setSecondName(request.getLastName());
            updateUserVerify.setCardFrontUrl(request.getCardFrontUrl());
            updateUserVerify.setCardBackUrl(request.getCardBackUrl());
            updateUserVerify.setCardNo(request.getCardNo());
            updateUserVerify.setKycLevel(request.getKycLevel());
            updateUserVerify.setCardType(request.getCardType());
            updateUserVerify.setCountryCode(request.getCountryCode());
            updateUserVerify.setGender(request.getGender());
            updateUserVerify.setVerifyStatus(request.getVerifyStatus());
            updateUserVerify.setKycLevel(kycLevel.getValue());
            updateUserVerify.setAccountId(request.getAccountId());
            updateUserVerify.setWorkFlowExecutionId(request.getWorkflowId());
            updateUserVerify.setUpdated(timestamp);

            UserVerify userVerifyEncrypted = UserVerify.encrypt(updateUserVerify);

            int kycApplyResult = userKycApplyMapper.updateJumioKycStatus(userVerifyDb.getUserId(),userVerifyEncrypted.getFirstName(),userVerifyEncrypted.getSecondName(),
                    userVerifyDb.getGender(),request.getCardType(),userVerifyDb.getKycLevel(),userVerifyDb.getVerifyStatus(),userVerifyEncrypted.getCardFrontUrl(),
                    userVerifyEncrypted.getCardBackUrl(),userVerifyEncrypted.getCardNo(),userVerifyDb.getAccountId(),timestamp);
            // update data for both tables
            int verifyMapperResult = userVerifyMapper.update(userVerifyEncrypted);
            if(kycApplyResult == 1 && verifyMapperResult ==1)
                return true;
            else
                return false;

        } catch (Exception e) {
            throw new RuntimeException("error updating user jumio data");
        }

    }
}
